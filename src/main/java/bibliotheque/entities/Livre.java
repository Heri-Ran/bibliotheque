package bibliotheque.entities;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Livre {
	private int idLivre;
	private String titre;
	private String auteur;
	private String edition;
	private Date dateSortie;
	private String preview;
}
