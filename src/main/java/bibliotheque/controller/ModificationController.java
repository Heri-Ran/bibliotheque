package bibliotheque.controller;

import java.sql.Date;

import bibliotheque.dao.ConnectDB;
import bibliotheque.dao.CrudDB;
import bibliotheque.entities.Livre;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ModificationController {

    @FXML
    private ComboBox<Integer> idModif;

    @FXML
    private TextField titreTextField;

    @FXML
    private TextField auteurTextField;

    @FXML
    private TextField editionTextField;

    @FXML
    private DatePicker dateSortiePicker;

    @FXML
    private TextField previewTextField;

    @FXML
    private Label msg;

    @FXML
    private Button saveModifButton;

    @FXML
    private Button cancelModifButton;

    @FXML
    void buttonsHandlerActions(ActionEvent event) throws Exception {
    	if(event.getSource().equals(cancelModifButton))
    		cancelModifButton.getScene().getWindow().hide();
    	
    	Livre taloha = (Livre) CrudDB.select("livre", "bibliotheque.entities", " WHERE idLivre="+idModif.getValue(), new ConnectDB("bibliotheque"))[0];
    	
    	Livre vaovao = new Livre(
    			idModif.getValue(),
    			titreTextField.getText(),
    			auteurTextField.getText(), 
    			editionTextField.getText(),
    			Date.valueOf(dateSortiePicker.getValue()),
    			previewTextField.getText()
    	);
    	
    	CrudDB.update(taloha, vaovao, new ConnectDB("bibliotheque"));
    	
    }
    
    @FXML
    void refreshData(ActionEvent event) {
    	try {
			getSelectedData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @FXML
    void initialize(){
    	try {
    		// Generation an'ilay id rehetra anaty combotbox
			Livre[] idList = (Livre[]) CrudDB.select("livre", "bibliotheque.entities", "", new ConnectDB("bibliotheque"));
			idModif.getItems().removeAll(idModif.getItems());
			
			
			ObservableList<Integer> listID = FXCollections.observableArrayList();
			
			for(int i = 0; i < idList.length; i += 1)
				listID.add(i, Integer.valueOf(idList[i].getIdLivre()));
			
			idModif.getItems().addAll(listID);
			idModif.getSelectionModel().select(0);
			
			// Generation des textField
			getSelectedData();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
    }
    
    void getSelectedData() throws Exception{
		Livre livre = (Livre) CrudDB.select("livre", "bibliotheque.entities", " WHERE idLivre="+idModif.getValue(), new ConnectDB("bibliotheque"))[0];
		
		titreTextField.setText(livre.getTitre());
		auteurTextField.setText(livre.getAuteur());
		editionTextField.setText(livre.getEdition());
		dateSortiePicker.setValue(livre.getDateSortie().toLocalDate());
		previewTextField.setText(livre.getPreview());
    }
    
    class CancelException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public CancelException(String msg){
    		super(msg);
    	}
    }
}
