package bibliotheque.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Date;
import java.time.Clock;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import com.dansoftware.pdfdisplayer.PDFDisplayer;

import bibliotheque.dao.ConnectDB;
import bibliotheque.dao.CrudDB;
import bibliotheque.entities.Livre;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainController {

    @FXML
    private Tab listTab;

    @FXML
    private AnchorPane listPane;

    @FXML
    private TableView<Livre> tableList;

    @FXML
    private TableColumn<Livre, Integer> idColumn;

    @FXML
    private TableColumn<Livre, String> titreColumn;

    @FXML
    private TableColumn<Livre, String> auteurColumn;

    @FXML
    private TableColumn<Livre, String> editionColumn;

    @FXML
    private TableColumn<Livre, String> dateSortieColumn;

    @FXML
    private TableColumn<Livre, String> lireColumn;

    @FXML
    private Button deleteLivre;

    @FXML
    private Button modifierLivre;

    @FXML
    private Tab ajouterTab;

    @FXML
    private AnchorPane ajouterPane;

    @FXML
    private Label titreLabel;

    @FXML
    private Label auteurLabel;

    @FXML
    private Label editionLabel;

    @FXML
    private Label datedesortieLabel;

    @FXML
    private Label choisirUnLivreLabel;

    @FXML
    private TextField tittreTextField;

    @FXML
    private TextField auteurTextField;

    @FXML
    private TextField editionTextField;
    
    @FXML
    private TextField searchBar;

    @FXML
    private DatePicker datedeSortiePicker;

    @FXML
    private Button choisirButton;

    @FXML
    private Button ajouterButton;

    @FXML
    private Button annulerButton;

    @FXML
    private Label msgLabel;
    
    @FXML
    private Label listMessage;
    
    @FXML
    private Button lireLivre;
    
    private ObservableList<Livre> listLivre;
    
    @FXML
    void addHandlerButtons(ActionEvent event) throws Exception {
    	try{
    		if(event.getSource().equals(annulerButton))
    			throw new AnnulerException("Ajanona hono");
    		
    		if(event.getSource().equals(modifierLivre))
    			throw new ModifierException("Amboarina");
    		
    		if(event.getSource().equals(deleteLivre))
    			throw new SupprimerException("Fafana");
    		
    		if(event.getSource().equals(lireLivre))
    			throw new LireException("Hamaky hono");
    		
    		String titre = tittreTextField.getText();
    		String auteur = auteurTextField.getText();
    		String edition = editionTextField.getText();
    		Date dateSortie = Date.valueOf(datedeSortiePicker.getValue());
    		String preview = choisirButton.getText();
    		
    		
    		if(titre.contains("'")){
    			String[] arr = titre.split("'");
    			titre = "";
    			for(int i = 0; i < arr.length; i += 1){
    				if(i + 1 == arr.length){
    					titre+=arr[i];
    					break;
    				}
    				titre += arr[i]+"\\'";
    			}
    		}
    		
    		
    		
    		Livre livre = new Livre(0, titre, auteur, edition, dateSortie, preview);
    		
    		CrudDB.insert(livre, "livre");
    		
    		msgLabel.setTextFill(Color.GREEN);
    		msgLabel.setText("Livre ajouter avec succes");
    		
    		initChnamp();
    		refreshTab();
    	}catch(AnnulerException ae){
    		initChnamp();
    		
    		msgLabel.setTextFill(Color.YELLOW);
    		msgLabel.setText("Op�ration annul�");
    		
    		refreshTab();
    	} catch (ModifierException e) {
    		
    		Parent modif = FXMLLoader.load(getClass().getResource("..//views/ModificationView.fxml"));
    		Stage modifStage = new Stage(StageStyle.UNDECORATED);   
    		
    	    Scene scene = new Scene(modif, 450, 650);

    	    modifStage.setResizable(false);
    	    modifStage.setTitle("Modification Livre");
    	    modifStage.setScene(scene);
    	    modifStage.show();
    	    
			System.out.println("Fen�tre de modification!");
			
			refreshTab();
		} catch (SupprimerException e) {
			try{
				Alert confirmation = new Alert(AlertType.CONFIRMATION);
					
				confirmation.setTitle("Demande de confirmation !");
				confirmation.setHeaderText(null);
				confirmation.setContentText("Cet action sera irreversible, voulez-vous continuer ?");
				Optional<ButtonType> reponse = confirmation.showAndWait();
					
				if(reponse.get() == ButtonType.CANCEL)
					throw new AnnulerException("Action annul�");
					
				Livre element = tableList.getSelectionModel().getSelectedItem();
				CrudDB.remove(element, new ConnectDB("bibliotheque"));	
				
				Files.delete(Paths.get("src/main/resources/livres/pdf/"+element.getPreview()));
				
				listMessage.setTextFill(Color.GREEN);
				listMessage.setText("Livre supprim� avec succ�s!");
					
				refreshTab();
			}catch(AnnulerException ae){
					listMessage.setText("");
			}catch(Exception e1){
				System.out.println("SupprimerElement :: "+e1.getMessage());
			}
		} catch(LireException le){
			try{
				Livre livre = tableList.getSelectionModel().getSelectedItem();
				String extension = livre.getPreview().substring(livre.getPreview().length() - 3);
				Stage pdfViewer = new Stage();
				
				if(extension.compareToIgnoreCase("pub") == 0)
					throw new Exception("epubs !");//extension = "epubs";
				
				PDFDisplayer displayer = new PDFDisplayer();
				pdfViewer.setScene(new Scene(displayer.toNode()));
				pdfViewer.setTitle("E-Read - "+livre.getTitre());
				pdfViewer.show();
				
				displayer.displayPdf(new File("src/main/resources/livres/"+extension+"/"+livre.getPreview()));
			}catch(NullPointerException npe){
				listMessage.setTextFill(Color.ORANGE);
				listMessage.setText("Veuillez choisir un livre");
			}
		} catch (NullPointerException ne){
			msgLabel.setTextFill(Color.ORANGE);
			msgLabel.setText("Veuillez remplir correctement les champs");
			
			refreshTab();
		}catch (Exception e) {
			refreshTab(); 
			e.printStackTrace();
		}
    }

    @FXML
    void searchWord(KeyEvent event) throws Exception {
    	String str = searchBar.getText();
    	
    	Livre[] liste = (Livre[]) CrudDB.select("livre", "bibliotheque.entities", " WHERE titre LIKE '%"+str+"%' OR auteur LIKE '%"+str+"%' OR edition LIKE '%"+str+"%'", new ConnectDB("bibliotheque"));
    	
    	tableList.getItems().removeAll(tableList.getItems());
    	
    	ObservableList<Livre> data = FXCollections.observableArrayList(Arrays.asList(liste));
    	
    	tableList.setItems(data);
    }
    
    @FXML
    void chooseFileToUpload(ActionEvent event) throws IOException {
    	FileChooser fileChooser = new FileChooser();
    	
    	fileChooser.getExtensionFilters().addAll(
    			new FileChooser.ExtensionFilter("PDF Document", "*.pdf"),
    			new FileChooser.ExtensionFilter("EPUB Files", "*.epub")
    	);
    	
    	File selectFile = fileChooser.showOpenDialog(new Stage());
    	
    	Path original = selectFile.toPath();
    	
    	Path cible = Paths.get("src/main/resources/livres/epubs/"+selectFile.getName());
    	if(selectFile.getName().substring(selectFile.getName().length() - 3).compareToIgnoreCase("pdf") == 0)
    		cible = Paths.get("src/main/resources/livres/pdf/"+selectFile.getName());
    	
    	Files.copy(original, cible, StandardCopyOption.REPLACE_EXISTING);
    	
    	choisirButton.setText(selectFile.getName());
    }    
    
    @FXML
    void initialize() {
        try {
        	refreshTab();
		} catch (Exception e) {
			e.printStackTrace();
		} 
    }
    
    void refreshTab() throws Exception{
		Livre[] livreArray = (Livre[]) CrudDB.select("livre", "bibliotheque.entities", "", new ConnectDB("bibliotheque"));
		listLivre = FXCollections.observableArrayList(Arrays.asList(livreArray));
		
		tableList.getItems().removeAll(tableList.getItems());
		
		initCell();
		
		tableList.setItems(listLivre);
		
    }
    
    void initChnamp() throws Exception{
    	  tittreTextField.setText("");
    	  auteurTextField.setText("");
    	  editionTextField.setText("");
    	  datedeSortiePicker.setValue(LocalDate.now(Clock.systemDefaultZone()));
    	  choisirButton.setText("Choisir un fichier ...");
    	  refreshTab();
    }
    
    void initCell(){
		idColumn.setCellValueFactory(new PropertyValueFactory<Livre, Integer>("idLivre"));
		titreColumn.setCellValueFactory(new PropertyValueFactory<Livre, String>("titre"));
		auteurColumn.setCellValueFactory(new PropertyValueFactory<Livre, String>("auteur"));
		editionColumn.setCellValueFactory(new PropertyValueFactory<Livre, String>("edition"));
		dateSortieColumn.setCellValueFactory(new PropertyValueFactory<Livre, String>("dateSortie"));
		lireColumn.setCellValueFactory(new PropertyValueFactory<Livre, String>("preview"));
    }
    
    class AnnulerException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public AnnulerException(String msg){
    		super(msg);
    	}
    }
    
    class ModifierException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ModifierException(String msg){
    		super(msg);
    	}
    }
    
    class SupprimerException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SupprimerException(String msg){
    		super(msg);
    	}
    }
    
    class LireException extends Exception{

		/**
		 * 
		 */
		private static final long serialVersionUID = 2075909868692679353L;
    	
		public LireException(String msg){
			super(msg);
		}
    }
}