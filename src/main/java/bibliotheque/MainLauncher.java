package bibliotheque;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainLauncher extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("views/MainView.fxml"));
		
	    Scene scene = new Scene(root, 1000, 600);
	    
	    scene.getStylesheets().add("bibliotheque/views/css/MainStyle.css");
	    
	    primaryStage.setResizable(false);
	    primaryStage.setTitle("E-Read");
	    primaryStage.setScene(scene);
	    primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
